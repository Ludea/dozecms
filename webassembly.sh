#!/bin/bash

EMCC_CORES=1 ../emscripten/emcc -lnodefs.js -s FORCE_FILESYSTEM=1 --bind -I/data/data/com.termux/files/home/sqlite3 -I/data/data/com.termux/files/home/emscripten/system/include /data/data/com.termux/files/home/emscripten/libsqlite3.a src/controller/*.cpp -o src/controller/storage.js -s EXPORT_ES6=1 -s MODULARIZE=1 -s WASM=1 -s EXPORT_NAME=Module -s USE_ES6_IMPORT_META=0

#Move wasm file into public folder
mv src/controller/storage.wasm public/storage.wasm

sed -i '1s;^;/* eslint-disable */\n;' src/controller/storage.js
