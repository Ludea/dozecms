#include "logger.h"

logger::logger()
{
    boost::log::core::get()->add_global_attribute("TimeStamp", boost::log::attributes::utc_clock());
    boost::log::add_common_attributes();
}

logger::~logger()
{
    //dtor
}

void logger::info(std::string message)
{
    logging::record rec = lg.open_record();
    if (rec)
    {
    	logging::record_ostream strm(rec);
    	strm << message;
    	strm.flush();
    	lg.push_record(boost::move(rec));
    }
}

void logger::fail(boost::system::error_code ec, char const* what)
{
    logging::core::get()->set_filter
    (
        logging::trivial::severity >= logging::trivial::error
    );
    logging::record rec = lg.open_record();
    if (rec)
    {
        logging::record_ostream strm(rec);
        strm << what << ": " << ec.message();                                                            strm.flush();
        lg.push_record(boost::move(rec));
    }
}
