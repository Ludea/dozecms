#include <boost/beast/core.hpp>

namespace beast = boost::beast;

// Return a reasonable mime type based based on the extension of a file.
beast::string_view mime_type(beast::string_view path) ;
