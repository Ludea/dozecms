#include <boost/beast/core.hpp>
#include <boost/asio/coroutine.hpp>
#include <boost/asio/strand.hpp>
#include <memory>

#include "session.h"
#include "logger.h"

namespace beast = boost::beast;
namespace net = boost::asio;
using tcp = boost::asio::ip::tcp;

// Accepts incoming connections and launches the sessions
class listener
    : public boost::asio::coroutine
    , public std::enable_shared_from_this<listener>
{
    net::io_context& ioc;
    tcp::acceptor acceptor;
    tcp::socket socket;
    std::shared_ptr<std::string const> doc_root;

public:
    listener(
        net::io_context& ioc,
        tcp::endpoint endpoint,
        std::shared_ptr<std::string const> const& doc_root);
       // Start accepting incoming connections
    void run();
private:
   void loop(beast::error_code ec = {});
   logger log;
};
