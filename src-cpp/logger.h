#ifndef LOGGER_H
#define LOGGER_H

#pragma once

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sinks/text_file_backend.hpp>

#include <boost/system/error_code.hpp>
#include <iostream>

namespace logging = boost::log;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;
namespace src = boost::log::sources;

class logger
{
    public:
        logger();
        ~logger();
        void fail(boost::system::error_code ec, char const* what);
	void info(std::string message);
    private:
	src::logger lg;
};

#endif // LOGGER_H
