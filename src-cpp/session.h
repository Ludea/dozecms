#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/asio/dispatch.hpp>

#include "request_handler.h"
#include "logger.h"

namespace beast = boost::beast;
namespace net = boost::asio;
namespace http = beast::http;
using tcp = boost::asio::ip::tcp;

// Handles an HTTP server connection
class session
    : public boost::asio::coroutine
    , public std::enable_shared_from_this<session>
{
    // This is the C++11 equivalent of a generic lambda.
    // The function object is used to send an HTTP message.
    struct send_lambda
    {
        session& self;
        std::shared_ptr<void> res;

        explicit
        send_lambda(session& self)
            : self(self)
        {
        }

        template<bool isRequest, class Body, class Fields>
        void
        operator()(http::message<isRequest, Body, Fields>&& msg) const
        {
            // The lifetime of the message has to extend
            // for the duration of the async operation so
            // we use a shared_ptr to manage it.
            auto sp = std::make_shared<
                http::message<isRequest, Body, Fields>>(std::move(msg));

            // Store a type-erased version of the shared
            // pointer in the class to keep it alive.
            self.res = sp;

            // Write the response
            http::async_write(
                self.stream,
                *sp,
                beast::bind_front_handler(
                    &session::loop,
                    self.shared_from_this(),
                    sp->need_eof()));
        }
    };

    beast::tcp_stream stream;
    beast::flat_buffer buffer;
    std::shared_ptr<std::string const> doc_root;
    http::request<http::string_body> req;
    std::shared_ptr<void> res;
    send_lambda lambda;
    logger log;
public:
    // Take ownership of the socket
   explicit session(tcp::socket&& socket, std::shared_ptr<std::string const> const& doc_root);
   void run();
   void loop(bool close, beast::error_code ec, std::size_t bytes_transferred);
};
