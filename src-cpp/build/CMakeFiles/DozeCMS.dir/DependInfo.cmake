# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/data/data/com.termux/files/home/dozecms/src-cpp/listener.cpp" "/data/data/com.termux/files/home/dozecms/src-cpp/build/CMakeFiles/DozeCMS.dir/listener.cpp.o"
  "/data/data/com.termux/files/home/dozecms/src-cpp/logger.cpp" "/data/data/com.termux/files/home/dozecms/src-cpp/build/CMakeFiles/DozeCMS.dir/logger.cpp.o"
  "/data/data/com.termux/files/home/dozecms/src-cpp/main.cpp" "/data/data/com.termux/files/home/dozecms/src-cpp/build/CMakeFiles/DozeCMS.dir/main.cpp.o"
  "/data/data/com.termux/files/home/dozecms/src-cpp/mime_type.cpp" "/data/data/com.termux/files/home/dozecms/src-cpp/build/CMakeFiles/DozeCMS.dir/mime_type.cpp.o"
  "/data/data/com.termux/files/home/dozecms/src-cpp/request_handler.cpp" "/data/data/com.termux/files/home/dozecms/src-cpp/build/CMakeFiles/DozeCMS.dir/request_handler.cpp.o"
  "/data/data/com.termux/files/home/dozecms/src-cpp/session.cpp" "/data/data/com.termux/files/home/dozecms/src-cpp/build/CMakeFiles/DozeCMS.dir/session.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ALL_NO_LIB"
  "BOOST_ATOMIC_DYN_LINK"
  "BOOST_CHRONO_DYN_LINK"
  "BOOST_FILESYSTEM_DYN_LINK"
  "BOOST_LOG_DYN_LINK"
  "BOOST_LOG_SETUP_DYN_LINK"
  "BOOST_REGEX_DYN_LINK"
  "BOOST_THREAD_DYN_LINK"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
