#include "listener.h"

listener::listener(
        net::io_context& ioc,
        tcp::endpoint endpoint,
        std::shared_ptr<std::string const> const& doc_root)
        : ioc(ioc)
        , acceptor(net::make_strand(ioc))
        , socket(net::make_strand(ioc))
        , doc_root(doc_root)
    {
        beast::error_code ec;

        // Open the acceptor
        acceptor.open(endpoint.protocol(), ec);
        if(ec)
        {
            log.fail(ec, "open");
            return;
        }

        // Allow address reuse
        acceptor.set_option(net::socket_base::reuse_address(true), ec);
        if(ec)
        {
            log.fail(ec, "set_option");
            return;
        }

        // Bind to the server address
        acceptor.bind(endpoint, ec);
        if(ec)
        {
            log.fail(ec, "bind");
            return;
        }

        // Start listening for connections
        acceptor.listen(net::socket_base::max_listen_connections, ec);
        if(ec)
        {
            log.fail(ec, "listen");
            return;
        }
    }

void listener::run()
{
    loop();
}

#include <boost/asio/yield.hpp>

void listener::loop(beast::error_code ec)
{
        reenter(*this)
        {
            for(;;)
            {
                yield acceptor.async_accept(
                    socket,
                    beast::bind_front_handler(
                        &listener::loop,
                        shared_from_this()));
                if(ec)
                {
                    log.fail(ec, "accept");
                }
                else
                {
                    // Create the session and run it
                    std::make_shared<session>(
                        std::move(socket),
                        doc_root)->run();
                }

                // Make sure each session gets its own strand
                socket = tcp::socket(net::make_strand(ioc));
            }
        }
}

    #include <boost/asio/unyield.hpp>
