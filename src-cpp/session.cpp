#include "session.h"

session::session(
        tcp::socket&& socket,
        std::shared_ptr<std::string const> const& doc_root)
        : stream(std::move(socket))
        , doc_root(doc_root)
        , lambda(*this)
    {
    }

void session::run()
{
        // We need to be executing within a strand to perform async operations
        // on the I/O objects in this session. Although not strictly necessary
        // for single-threaded contexts, this example code is written to be
        // thread-safe by default.
        net::dispatch(stream.get_executor(),
                      beast::bind_front_handler(&session::loop,
                                                shared_from_this(),
                                                false,
                                                beast::error_code{},
                                                0));
}

#include <boost/asio/yield.hpp>

    void
    session::loop(
        bool close,
        beast::error_code ec,
        std::size_t bytes_transferred)
    {
        boost::ignore_unused(bytes_transferred);
        reenter(*this)
        {
            for(;;)
            {
                // Make the request empty before reading,
                // otherwise the operation behavior is undefined.
                req = {};

                // Set the timeout.
                stream.expires_after(std::chrono::seconds(30));

                // Read a request
                yield http::async_read(stream, buffer, req,
                    beast::bind_front_handler(
                        &session::loop,
                        shared_from_this(),
                        false));
                if(ec == http::error::end_of_stream)
                {
                    // The remote host closed the connection
                    break;
                }
                if(ec)
                    return log.fail(ec, "read");

                // Send the response
                yield handle_request(*doc_root, std::move(req), lambda);
                if(ec)
                    return log.fail(ec, "write");
                if(close)
                {
                    // This means we should close the connection, usually because
                    // the response indicated the "Connection: close" semantic.
                    break;
                }

                // We're done with the response so delete it
                res = nullptr;
            }

            // Send a TCP shutdown
            stream.socket().shutdown(tcp::socket::shutdown_send, ec);

            // At this point the connection is closed gracefully
        }
    }

    #include <boost/asio/unyield.hpp>


