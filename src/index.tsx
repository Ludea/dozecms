import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Switch} from "react-router-dom";
import { createBrowserHistory } from "history";

import Admin from "layouts/Admin";
import Index from "views/Index";
import reportWebVitals from './reportWebVitals';

const hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route exact path="/" component={Index} />
      <Route path="/admin" component={Admin} />
    </Switch>
  </Router>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
