#include <sqlite3.h>
#include <iostream>
#include <emscripten.h>
#include <emscripten/bind.h>

using namespace emscripten;

class Storage
{
public:
      Storage();
      std::string open_database();
      void close_database();
      void set_data(std::string title, std::string content);
      std::string get_data(std::string columName);
private:
      std::string create_table();
      static int callback(void *NotUsed, int argc, char **argv, char **azColName);
      sqlite3 *db;
      int rc;
      char *errorMessage;
      sqlite3_stmt *result;
      std::string sql_request;
};
