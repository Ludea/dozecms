#include "storage.h"

Storage::Storage()
{
}

std::string Storage::open_database()
{
   if (sqlite3_open("Doze.db", &db) == SQLITE_OK )
   {
	/*if (SQLITE_OK  == create_table())
	{
	    return 0;
	}
	else
	{
	   return 1;
	}*/
	return create_table();
   }
   else
	return "wrong";
}

void Storage::close_database()
{
    sqlite3_close(db);
}

std::string Storage::create_table()
{
    std::string t;
    char *sql = "CREATE TABLE IF NOT EXISTS news(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title TEXT NOT NULL, content TEXT NOT NULL, author, TEXT NOT NULL, date date, time TIME)" ;
    rc = sqlite3_prepare_v2(db, sql, -1, &result ,0);
    if ( SQLITE_OK == sqlite3_exec(db, sql, callback, 0, &errorMessage))
    {
	t = errorMessage;
	sqlite3_free(errorMessage);
	return "ok" ;
    }
    else
    {
	t = errorMessage;
	sqlite3_free(errorMessage);
	return t ;
    }
}

int Storage::callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i;
    for(i=0; i<argc; i++)
    {
      std::cout << "%s = %s\n" << azColName[i] << " " << argv[i] << std::endl;
    }
    return 0;
}

void Storage::set_data(std::string title, std::string content)
{
    sql_request = "INSERT INTO news(title, content) VALUES (" + title+ ", " + content + ");";
    rc = sqlite3_prepare_v2(db, sql_request.c_str(), -1, &result, 0);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
    }

    rc = sqlite3_step(result);
}

std::string Storage::get_data(std::string columName)
{
    sql_request = "SELECT " + columName + " FROM news ;" ;
    rc = sqlite3_prepare_v2(db, sql_request.c_str(), -1, &result, 0);

    if (rc != SQLITE_OK)
    {
        sqlite3_close(db);
	std::string error = "Failed to fetch data : " + std::string(sqlite3_errmsg(db)) ;
	return error ;
    }

    rc = sqlite3_step(result);
    if (rc == SQLITE_ROW) {
	std::string content = std::string( reinterpret_cast< const char* >(sqlite3_column_text(result, 0)));
    	sqlite3_finalize(result);
    	sqlite3_close(db);
	return content ;
    }
    else
	return "nada";
}

EMSCRIPTEN_BINDINGS(storage) {
  class_<Storage>("Storage")
    .constructor()
    .function("close_database", &Storage::close_database)
    .function("open_database", &Storage::open_database)
    .function("set_data", &Storage::set_data)
    .function("get_data", &Storage::get_data)
    ;
}
